// BEGIN
require('expect-puppeteer');
const getApp = require('../server/index');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

const app = getApp();

describe('test on working the blog', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
  });

  beforeEach(async () => {
    await page.goto(appUrl);
  });

  it('show main page', async () => {
    expect(await page.$('[data-testid="nav-articles-index-link"]')).toBeTruthy();
  });

  it('show list articels', async () => {
    await page.goto(appArticlesUrl);
    await expect(await page.$('tr')).toBeTruthy();
  });

  it('create article', async () => {
    await page.goto(appArticlesUrl);
    const title = await page.$('h3');
    const pageTitle = await page.evaluate((element) => element.innerText, title);
    expect(pageTitle).toContain('Articles');
    await page.click('[data-testid="article-create-link"]');
    await page.waitForSelector('form');
    expect(await page.$eval('h3', (el) => el.textContent)).toEqual('Create article');
  });

  it('edit artcile', async () => {
    await page.goto(`${appArticlesUrl}/4/edit`);
    const input = await page.$('#name');
    await input.click({ clickCount: 3 });
    await input.type('foo');
    await page.click('[data-testid="article-update-button"]');
    await page.waitForSelector('tr');
    const articleFields = await page.$$eval(
      'tr > td',
      (fields) => fields.map((el) => el.textContent),
    );
    expect(articleFields).toContain('foo');
  });

  afterAll(() => {
    app.close();
  });
});
// END
