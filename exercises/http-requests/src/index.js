const axios = require('axios');

const api = axios.default.create({
  baseURL: 'https://httpbin.org',
  timeout: 1000,
  headers: {
    accept: 'application/json',
  },
});

// BEGIN
const get = async (path) => api.get(path);

const post = async (path) => api.post(path);
// END

module.exports = { get, post };
