const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');

// BEGIN
describe('test ajax via axios library', () => {
  beforeAll(() => {
    nock.disableNetConnect();
  });

  it('check get request in /get endpoint', async () => {
    const products = [
      { id: 1, name: 'Chips' },
      { id: 2, name: 'Fish' },
    ];
    nock('https://httpbin.org').get('/get').reply(200, products);
    const { data } = await get('get');
    expect(data).toEqual(products);
  });

  it('check post request in /post endpoint', async () => {
    const products = [
      { id: 1, name: 'Chips' },
      { id: 2, name: 'Fish' },
    ];
    nock('https://httpbin.org').post('/post').reply(200, products);
    const { data } = await post('post');
    expect(data).toEqual(products);
  });

  it('check get request with 404 code', async () => {
    await expect(
      get('foo'),
    ).rejects.toThrow();
  });

  afterAll(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });
});
// END
