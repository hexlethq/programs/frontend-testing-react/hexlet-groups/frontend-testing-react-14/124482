const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

const sumArray = (arr) => arr.reduce((acc, el) => acc + el, 0);

// BEGIN
describe('test sort array', () => {
  it('should contain same length', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted.length).toEqual(data.length);
      }),
    );
  });

  it('should produce ordered array', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      }),
    );
  });

  it('should have idempotent behavior', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(sort(data));
        expect(sorted).toEqual(sort(data));
      }),
    );
  });

  it('should contain same summary', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sumArray(sorted)).toEqual(sumArray(data));
      }),
    );
  });

  it('should contain same element in inversion', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sortDesc = (dataDesc) => dataDesc.slice().sort((a, b) => a - b);
        const sorted = sort(sortDesc(data));
        expect(sorted).toEqual(sort(data));
      }),
    );
  });
});
// END
