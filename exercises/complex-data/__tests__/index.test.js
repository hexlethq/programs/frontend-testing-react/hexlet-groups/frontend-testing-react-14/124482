const faker = require('faker');

// BEGIN
describe('test helper.createTransaction in faker', () => {
  it('generate right structure transaction', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toHaveProperty('date', 'business', 'name', 'type', 'account');
  });

  it('check date format in generate transaction', () => {
    const transaction = faker.helpers.createTransaction();
    const date = new Date(transaction.date);
    expect(transaction.date).toEqual(date);
  });

  it('check amount with positive number', () => {
    const transaction = faker.helpers.createTransaction();
    expect(parseFloat(transaction.amount)).toBeGreaterThanOrEqual(0);
  });

  it('check account with positive number', () => {
    const transaction = faker.helpers.createTransaction();
    expect(parseInt(transaction.account, 10)).toBeGreaterThanOrEqual(0);
  });

  it('check generate unique transaction', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction1).not.toMatchObject(transaction2);
    expect(transaction1).not.toBe(transaction2);
  });
});
// END
