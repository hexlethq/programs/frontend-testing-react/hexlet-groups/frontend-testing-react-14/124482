// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
it('check empty list', () => {
  expect(document.querySelectorAll('[data-container="lists"] li').length).toEqual(1);
  expect(document.querySelector('[data-container="tasks"]')).toBeEmptyDOMElement();
});

it('add task', async () => {
  document.querySelector('[data-testid="add-task-input"]').value = 'foo';
  document.querySelector('[data-testid="add-task-button"]').click();
  expect(document.querySelector('[data-container="tasks"]')).toHaveTextContent('foo');
  expect(document.querySelector('[data-testid="add-task-input"]')).toBeEmptyDOMElement();

  document.querySelector('[data-testid="add-task-input"]').value = 'foo 2';
  document.querySelector('[data-testid="add-task-button"]').click();
  expect(document.querySelector('[data-container="tasks"]')).toHaveTextContent('foo');
  expect(document.querySelector('[data-container="tasks"]')).toHaveTextContent('foo 2');
  expect(document.querySelector('[data-testid="add-task-input"]')).toBeEmptyDOMElement();
});

it('add list', () => {
  document.querySelector('[data-testid="add-list-input"]').value = 'foo';
  document.querySelector('[data-testid="add-list-button"]').click();
  expect(document.querySelector('[data-container="lists"]')).toHaveTextContent('foo');
  expect(document.querySelector('[data-testid="add-list-input"]')).toBeEmptyDOMElement();

  document.querySelector('[data-testid="add-list-input"]').value = 'foo 2';
  document.querySelector('[data-testid="add-list-button"]').click();
  expect(document.querySelector('[data-container="lists"]')).toHaveTextContent('foo');
  expect(document.querySelector('[data-container="lists"]')).toHaveTextContent('foo 2');
  expect(document.querySelector('[data-testid="add-list-input"]')).toBeEmptyDOMElement();
});
// END
