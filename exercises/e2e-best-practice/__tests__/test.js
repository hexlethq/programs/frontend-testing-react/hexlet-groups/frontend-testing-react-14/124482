// @ts-check
// BEGIN
jest.setTimeout(30000);

describe('Task app', () => {
  beforeEach(async () => {
    await page.goto('http://0.0.0.0:8080');
  });

  it('check first element', async () => {
    await expect(page).toMatchElement('input[data-testid="task-name-input"]');
    await expect(page).toMatchElement('input[data-testid="add-task-button"]');
  });

  it('should create and delete task', async () => {
    const text = 'text';

    await page.waitForSelector('[data-testid=add-task-button]');

    await expect(page).toFillForm('form', { text });

    await page.click(('input[data-testid="add-task-button"]'));

    await expect(page).toMatch(text);

    await page.click('[data-testid=remove-task-1]');
    await expect(page).not.toMatch(text);
  });
});
// END
