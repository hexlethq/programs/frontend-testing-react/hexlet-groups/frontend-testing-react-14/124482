const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, [3, [4]], 5]];
const answerArr = [1, 2, 3, 4, 5];
const result = flattenDepth(array, Infinity);
assert.deepEqual(result, answerArr, 'flatten array with Infinity arg');
assert.deepEqual(flattenDepth([], null), [], 'empty array with null arg');
assert.deepEqual(flattenDepth(array, 0), array, 'initial array');
assert.deepEqual(flattenDepth(array, 1), [1, 2, [3, [4]], 5], 'flat one level');
assert.deepEqual(flattenDepth(array, 'opa'), array, 'pass string arg');
assert.deepEqual(flattenDepth(array, 20), answerArr, 'pass more lvl than existing');
// END
