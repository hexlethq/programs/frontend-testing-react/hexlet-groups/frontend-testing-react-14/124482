// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
it('check init state', () => {
  expect(document.querySelector('[data-container="tasks"]')).toBeEmptyDOMElement();
  expect(document.querySelectorAll('[data-container="lists"] li').length).toBe(1);
  expect(screen.getByText('General')).toBeVisible();
});

it('add task', () => {
  const inputTask = screen.getByLabelText('New Task Name');
  userEvent.type(inputTask, 'foo');
  userEvent.click(screen.getByText('Add Task'));
  expect(inputTask.value).toEqual('');
  expect(screen.getByText('foo')).toBeVisible();

  userEvent.type(inputTask, 'foo 2');
  userEvent.click(screen.getByText('Add Task'));
  expect(screen.getByText('foo')).toBeVisible();
  expect(screen.getByText('foo 2')).toBeVisible();
});

it('add list', () => {
  const inputList = screen.getByLabelText('New List Name');
  userEvent.type(inputList, 'foo');
  userEvent.click(screen.getByText('Add List'));
  expect(inputList.value).toEqual('');
  expect(screen.getByText('foo')).toBeVisible();

  userEvent.type(inputList, 'foo 2');
  userEvent.click(screen.getByText('Add List'));
  expect(screen.getByText('foo')).toBeVisible();
  expect(screen.getByText('foo 2')).toBeVisible();
});
// END
