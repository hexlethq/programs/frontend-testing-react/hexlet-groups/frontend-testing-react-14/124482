// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

// BEGIN
afterAll(() => {
  nock.enableNetConnect();
});

it('search countries', async () => {
  const scope = nock(host).get('/countries?term=r').reply(200, ['Réunion', 'Romania', 'Russian Federation', 'Rwanda']);
  render(<Autocomplete/>);
  userEvent.type(screen.getByRole('textbox'), 'r');
  await waitFor(() => {
    expect(screen.queryByText('Réunion')).toBeVisible();
    expect(screen.queryByText('Romania')).toBeVisible();
    expect(screen.queryByText('Russian Federation')).toBeVisible();
    expect(screen.queryByText('Rwanda')).toBeVisible();
  });
  scope.done();
});

it('clean input', async () => {
  const scope = nock(host).get('/countries?term=r').reply(200, ['Réunion', 'Romania', 'Russian Federation', 'Rwanda']);
  nock(host).get('/countries?term=ru').reply(200, ['Russian Federation']);
  const {container} = render(<Autocomplete/>);
  const input = screen.getByRole('textbox');
  userEvent.type(input, 'ru');
  await waitFor(() => expect(screen.queryByText('Russian Federation')).toBeVisible());
  userEvent.clear(input);
  expect(container.querySelector('ul')).not.toBeInTheDocument();
});
// END
