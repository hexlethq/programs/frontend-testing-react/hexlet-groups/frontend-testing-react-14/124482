const fs = require('fs');
const path = require('path');

const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
const readFile = (filename) => fs.readFileSync(filename, 'utf-8');
const writeFile = (filename, data) => fs.writeFileSync(filename, data, 'utf-8');

// BEGIN
const upVersion = (pathArg, versionArg = 'patch') => {
  try {
    const pathFixture = getFixturePath(pathArg);
    const data = readFile(pathFixture);
    const { version } = JSON.parse(data);
    const mapSemVersion = {
      major: 0,
      minor: 1,
      patch: 2,
    };
    const upSemver = (versionSemver) => parseInt(versionSemver, 10) + 1;
    const versionResult = version.split('.')
      .map((versionSemver, indexSemver) => (
        mapSemVersion[versionArg] === indexSemver
          ? upSemver(versionSemver)
          : versionSemver)).join('.');
    writeFile(pathFixture, JSON.stringify({ version: versionResult }));
  } catch (error) {
    throw new Error('path is not exist');
  }
};
// END

module.exports = { upVersion };
