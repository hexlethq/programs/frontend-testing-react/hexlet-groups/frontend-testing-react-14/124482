const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const initialState = { version: '1.3.2' };
const answerPatch = { version: '1.3.3' };
const answerMinor = { version: '1.4.2' };
const answerMajor = { version: '2.3.2' };

const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
const readFile = (filename) => fs.readFileSync(filename, 'utf-8');
const writeFile = (filename, data) => fs.writeFileSync(filename, data, 'utf-8');

describe('upVersion', () => {
  afterEach(() => {
    const pathFixture = getFixturePath('package.json');
    writeFile(pathFixture, JSON.stringify(initialState));
  });

  it('test patch version', () => {
    upVersion('package.json');
    const result = JSON.parse(readFile(getFixturePath('package.json')));
    expect(result).toEqual(answerPatch);
  });

  it('test minor version', () => {
    upVersion('package.json', 'minor');
    const result = JSON.parse(readFile(getFixturePath('package.json')));
    expect(result).toEqual(answerMinor);
  });

  it('test major version', () => {
    upVersion('package.json', 'major');
    const result = JSON.parse(readFile(getFixturePath('package.json')));
    expect(result).toEqual(answerMajor);
  });

  it('test with no valid path', () => {
    expect(() => {
      upVersion('packageFoo.json');
    }).toThrow('path is not exist');
  });
});
// END
