describe('test object assign', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);
  const notWritable = Object.defineProperty({}, 'prop', {
    value: 'foo',
    writable: false,
  });

  it('compare by value objects', () => {
    const rightAnswer = { k: 'v', a: 'a', b: 'b' };
    expect(result).toEqual(rightAnswer);
  });

  it('check return same reference', () => {
    expect(result).toBe(target);
  });

  it('check not writable property', () => {
    notWritable.prop = 1;
    expect(notWritable.prop).toEqual('foo');
  });
});
