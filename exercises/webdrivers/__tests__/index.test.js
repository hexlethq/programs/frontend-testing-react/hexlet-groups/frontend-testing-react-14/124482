const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;
let browser;

/**
   * @type {Browser}
   */
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  it('e2e test with puppeteer', async () => {
    await page.goto(appUrl);
    await page.goto(appArticlesUrl);
    const articles = await page.$('#articles');
    expect(articles).not.toBeNull();
    await page.click('.container.mt-3 > a');
    await page.waitForSelector('form');
    expect(await page.$eval('h3', (el) => el.textContent)).toEqual('Create article');
    await page.type('#name', 'foo');
    await page.select('#category', '2');
    await page.type('#content', 'foo');
    await page.click('[type=submit]');
    await page.waitForSelector('tbody');
    const listTds = await page.$$eval('tbody > tr > td', (element) => element.map((el) => el.textContent));
    expect(listTds).toContain('foo');

    await page.click('td > a');
    await page.waitForSelector('form');
    await page.evaluate(() => {
      document.querySelector('#name').value = 'bar';
    });
    await page.click('[type=submit]');
    await page.waitForSelector('tbody');
    const listTdsAgain = await page.$$eval('tbody > tr > td', (element) => element.map((el) => el.textContent));
    expect(listTdsAgain).toContain('bar');
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
